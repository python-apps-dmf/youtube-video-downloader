# Youtube Video Downloader

This python project is meant to store code that can create an executable desktop application with a GUI to download Youtube videos. The user should be able to input the URL to the video they want to download, get some information on the video, so that they can confirm whether it is the desired one, and be able to download it.

## Operations supported/to support

Supported:

- Input Youtube video URL
- Display Youtube video information and metadata
- Choose destination folder and file name
- Download Youtube video
- ...

To Support:


- Progress bar

## Packages in use

- pytube
- customtkinter
- CTkMessagebox
- requests
- Pillow
- inflect

## Structure

- `/youtube-video-downloader` - root directory 
  - `/src` - contains the application source code
  - `/notebooks` - contains the Jupyter notebooks used to create the application
  - `/data` - contains the notebook experiment data

## How to use

1. First clone the project
2. Create a python virtual environment in the root folder `python -m venv .venv`
3. Turn the virtual environment on with `.\.venv\Scripts\activate`
4. Install the project dependencies with `pip install -r .\requirements.txt`
   1. If prompted, you can upgrade pip with `python -m pip install --upgrade pip`
5. Run the command `python -m src.main`, this should start the application
   1. Note: you may have to change the alias used to run python depending on your OS (like 'python3')
