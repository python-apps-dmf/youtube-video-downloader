import customtkinter as ctk
import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime

from .windows import MainWindow

if __name__ == "__main__":
    """Configuring the logging system"""
    log_formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    log_handler = TimedRotatingFileHandler(f'./logs/systemOut_{datetime.now().strftime("%Y-%m-%d")}.log',
                                           when='midnight', interval=1, backupCount=7, encoding='utf-8')
    log_handler.setFormatter(log_formatter)
    logging.basicConfig(level=logging.INFO, handlers=[log_handler])

    """Configuring Basic Appearance"""
    ctk.set_appearance_mode("dark")
    ctk.set_default_color_theme("green")

    """Starting the application"""
    app = MainWindow()
    app.mainloop()
