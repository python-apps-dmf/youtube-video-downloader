from ..utils.constants import WIDTH, HEIGHT


def center_and_size_window(window, width=WIDTH, height=HEIGHT):
    # Get screen size
    screen_width = window.winfo_screenwidth()
    screen_height = window.winfo_screenheight()

    # Calculate position
    x = screen_width // 2
    y = screen_height // 2

    # Set size and position of the window
    window.geometry(f'{width}x{height}+{x}+{y}')
