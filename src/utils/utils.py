from datetime import timedelta, datetime
import inflect


def convert_seconds_to_youtube_video_time(seconds: int):
    return str(timedelta(seconds=seconds))


def format_youtube_published_date(date):
    # Initialize the inflect engine
    p = inflect.engine()
    # Get the current date
    now = datetime.now()
    # Calculate the difference between the current date and the parsed date
    diff = now - date
    # Format the date
    date_str = date.strftime('{} of {} {}'.format(p.ordinal(date.day), date.strftime('%B'), date.year))
    if not (date.hour == 0 and date.minute == 0 and date.second == 0):
        date_str += date.strftime(' at {:02d}:{:02d}'.format(date.hour, date.minute))
    if diff.days != 0:
        if diff.days < 7:
            date_str += ' ({} days ago)'.format(diff.days)
        elif diff.days < 30:
            date_str += ' ({} weeks ago)'.format(diff.days // 7)
        elif diff.days < 365:
            date_str += ' ({} months ago)'.format(diff.days // 30)
        else:
            date_str += ' ({} years ago)'.format(diff.days // 365)
    return date_str


def format_youtube_video_views_number(views: int):
    if views >= 1e9:
        return '{:.1f}B'.format(views / 1e9)
    elif views >= 1e6:
        return '{:.1f}M'.format(views / 1e6)
    elif views >= 1e3:
        return '{:.1f}K'.format(views / 1e3)
    else:
        return '{}'.format(int(views))
