import os
from io import BytesIO
from urllib.parse import urlparse, parse_qs

import requests
from PIL import Image, ImageDraw, ImageFont
from pytube import YouTube


def validate_youtube_video_link(link: str):
    url_data = urlparse(link)
    host = url_data.netloc
    path = url_data.path
    query = parse_qs(url_data.query)

    if (host == 'youtu.be' or
            (host in ['youtube.com', 'www.youtube.com'] and ('v' in query or path.startswith('/shorts/')))):
        return True
    else:
        return False


def open_youtube_video_connection(link: str):
    return YouTube(link)


def get_video_metadata(video: YouTube):
    return {
        'title': video.title,
        'author': video.author,
        'length': video.length,
        'views': video.views,
        'publication_date': video.publish_date,
        'thumbnail_url': video.thumbnail_url,
        'description': video.description
    }


def get_thumbnail_image(link: str, width: int, length: str):
    response = requests.get(link)
    img = Image.open(BytesIO(response.content))
    aspect_ratio = img.height / img.width
    new_height = int(width * aspect_ratio)
    img = img.resize((width, new_height))

    _draw_image(img, length)

    return img


def download_video(video: YouTube, download_file_path: str):
    download_path, download_filename = os.path.split(download_file_path)
    video.streams.get_highest_resolution().download(output_path=download_path, filename=download_filename)


def _draw_image(img: Image, text: str):
    # Create a Draw object
    draw = ImageDraw.Draw(img)

    # Specify the font, size, and color
    font_size = 15
    font = ImageFont.truetype("arial.ttf", font_size)
    fill_color = "white"
    outline_color = "black"

    # Calculate the position of the text
    text_width = draw.textlength(text, font=font)
    pos = (img.width - text_width - 20, img.height - font_size - 10)

    # Draw the outline
    draw.rectangle((pos[0] - 5, pos[1] - 5, pos[0] + text_width + 5, pos[1] + font_size + 5), fill=outline_color)
    # Draw the text
    draw.text(pos, text, fill=fill_color, font=font)
