from tkinter import filedialog
import customtkinter as ctk
import logging
from CTkMessagebox import CTkMessagebox
from pytube.helpers import safe_filename

from youtube_operations import validate_youtube_video_link, open_youtube_video_connection, get_video_metadata, \
    get_thumbnail_image, download_video


class MainWindow(ctk.CTk):
    def __init__(self, *args, **kwargs):
        ctk.CTk.__init__(self, *args, **kwargs)
        from ..utils import WIDTH, HEIGHT, TITLE_FONT, DESC_FONT, SMALL_DESC_FONT, BUTTON_FONT, center_and_size_window
        self.title("Youtube Video Downloader")
        center_and_size_window(self)  # Set the window size and position

        """Create a scrollable frame"""
        self.frame = ctk.CTkScrollableFrame(self, orientation="vertical", width=WIDTH, height=HEIGHT, fg_color="gray14")
        self.frame.pack()

        """Common variables required to store necessary information"""
        self.youtube_object = None
        self.default_filename = None
        self.large_text_wrap_length = WIDTH - 50

        """Window UI"""
        """Opening message"""
        self.welcome_label = ctk.CTkLabel(self.frame, text="Welcome", font=TITLE_FONT)
        self.instruction_label = ctk.CTkLabel(self.frame, font=DESC_FONT, wraplength=self.large_text_wrap_length,
                                              text="Please paste the URL for the Youtube video you want to download")

        """Search for the video via link"""
        self.video_link_entry = ctk.CTkEntry(self.frame, placeholder_text="Video URL")
        self.video_link_entry.bind(sequence='<KeyRelease>', command=self.validate_search_button)
        self.search_video_button = ctk.CTkButton(self.frame, text="Search for Video", command=self.search_for_video,
                                                 font=BUTTON_FONT, width=150, state=ctk.DISABLED)

        """Showing video metadata"""
        self.video_confirmation_label = ctk.CTkLabel(self.frame, text="Is this the video you wanted?", font=DESC_FONT)
        self.video_title = ctk.CTkLabel(self.frame, font=TITLE_FONT, wraplength=self.large_text_wrap_length)
        self.video_author = ctk.CTkLabel(self.frame, font=DESC_FONT, wraplength=self.large_text_wrap_length)
        self.video_thumbnail = ctk.CTkLabel(self.frame)
        self.metadata_line_frame = ctk.CTkFrame(self.frame, fg_color="gray14", width=self.large_text_wrap_length)
        self.video_views = ctk.CTkLabel(self.metadata_line_frame, font=SMALL_DESC_FONT)
        self.video_release_date = ctk.CTkLabel(self.metadata_line_frame, font=SMALL_DESC_FONT)
        self.video_description = ctk.CTkLabel(self.frame, font=SMALL_DESC_FONT)

        """Decision buttons"""
        self.video_verification_cancellation_button = ctk.CTkButton(self.frame, text="No, let me try again",
                                                                    font=BUTTON_FONT, hover_color="#B22222",
                                                                    fg_color="#FF6347",
                                                                    command=self.video_search_cancellation)
        self.video_verification_confirmation_button = ctk.CTkButton(self.frame, text="Yes, let me download",
                                                                    font=BUTTON_FONT, command=self.video_download)

        """Building the UI"""
        self.welcome_label.pack(pady=20)
        self.instruction_label.pack(pady=10)
        self.video_link_entry.pack(pady=10, fill=ctk.X, padx=25)
        self.video_link_entry.focus()
        self.search_video_button.pack(pady=15, ipady=7.5)

        self.protocol("WM_DELETE_WINDOW", self.on_close)

        logging.info("Started the Youtube Video Downloader app")

    def on_close(self):
        logging.info("Shutting down the app")
        self.destroy()

    def validate_search_button(self, *args):
        if self.video_link_entry.get() and validate_youtube_video_link(self.video_link_entry.get()):
            logging.debug("User input a valid youtube video URL format...")
            self.search_video_button.configure(state=ctk.NORMAL)
        else:
            self.search_video_button.configure(state=ctk.DISABLED)

    def search_for_video(self):
        logging.debug("Searching for video...")
        if self.video_link_entry.get():
            logging.info(f"Searching for the video {self.video_link_entry.get()}")
            try:
                self.youtube_object = open_youtube_video_connection(self.video_link_entry.get())
                self.hide_search_fields()
                self.show_video_metadata()
            except Exception as e:
                logging.exception(f"An error occurred while searching for the video: {e}")
                _ = CTkMessagebox(title="Error", icon="cancel", option_1="OK",
                                  message="Something went wrong when trying to search for the Youtube video!")
                _.wait_window(_)
                self.clean_inputs()

    def hide_search_fields(self):
        logging.debug("Hiding search fields...")
        self.welcome_label.pack_forget()
        self.instruction_label.pack_forget()
        self.video_link_entry.pack_forget()
        self.search_video_button.pack_forget()

    def show_video_metadata(self):
        logging.debug("Showing video metadata...")
        metadata = get_video_metadata(self.youtube_object)

        self.video_confirmation_label.pack()

        """Title"""
        self.video_title.configure(text=f"{metadata['title']}")
        self.video_title.pack()
        self.default_filename = metadata['title']

        """Author"""
        self.video_author.configure(text=f"by {metadata['author']}")
        self.video_author.pack(pady=10)

        """Thumbnail & Length"""
        from ..utils import convert_seconds_to_youtube_video_time, format_youtube_published_date, \
            format_youtube_video_views_number
        length = convert_seconds_to_youtube_video_time(metadata['length'])
        thumbnail_image = get_thumbnail_image(metadata['thumbnail_url'], self.large_text_wrap_length, length)
        self.video_thumbnail.configure(
            image=ctk.CTkImage(dark_image=thumbnail_image, size=(thumbnail_image.width, thumbnail_image.height)),
            text="")
        self.video_thumbnail.pack(padx=25)

        """Views"""
        self.video_views.configure(text=f"{format_youtube_video_views_number(metadata['views'])} views")
        self.video_views.pack(side=ctk.LEFT, padx=(25, 0), pady=5)

        """Release Date"""
        self.video_release_date.configure(
            text=f"Released on the {format_youtube_published_date(metadata['publication_date'])}")
        self.video_release_date.pack(side=ctk.RIGHT, padx=(0, 25), pady=5)

        self.metadata_line_frame.pack(fill=ctk.X)

        """Description"""
        if metadata['description']:
            self.video_description.configure(text=f"Description: {metadata['description']}")
            self.video_description.pack()

        self.video_verification_cancellation_button.pack(side=ctk.LEFT, pady=(15, 0), ipady=7.5)
        self.video_verification_confirmation_button.pack(side=ctk.RIGHT, pady=(15, 0), ipady=7.5)

    def show_search_fields(self):
        logging.debug("Showing search fields...")
        self.clean_inputs()
        self.instruction_label.pack(pady=10)
        self.video_link_entry.pack(pady=10, fill=ctk.X, padx=25)
        self.video_link_entry.focus()
        self.search_video_button.pack(pady=15, ipady=7.5)

    def video_search_cancellation(self):
        logging.info("User chose to cancel the current video search")
        self.clean_metadata()
        self.show_search_fields()
        self.frame._parent_canvas.yview_moveto(0)

    def video_download(self):
        logging.info("User chose to download the current video")
        save_as_file = filedialog.asksaveasfilename(defaultextension=".mp4", initialfile=f"{self.default_filename}",
                                                    filetypes=[("MP4 files", "*.mp4")])
        if save_as_file != '':
            logging.info(f"Video will be saved to {save_as_file}")
            download_video(self.youtube_object, save_as_file)
            _ = CTkMessagebox(title="Success", message="The video was downloaded successfully!", icon="check",
                              option_1="OK")
            _.wait_window(_)
            self.clean_metadata()
            self.show_search_fields()

    def clean_inputs(self):
        logging.debug("Cleaning input fields...")
        self.video_link_entry.delete(0, ctk.END)
        self.youtube_object = None
        self.default_filename = None

    def clean_metadata(self):
        logging.debug("Cleaning video metadata fields...")
        self.video_confirmation_label.pack_forget()
        self.video_title.pack_forget()
        self.video_author.pack_forget()
        self.video_thumbnail.pack_forget()
        self.video_views.pack_forget()
        self.video_release_date.pack_forget()
        self.metadata_line_frame.pack_forget()
        self.video_description.pack_forget()
        self.video_verification_cancellation_button.pack_forget()
        self.video_verification_confirmation_button.pack_forget()
